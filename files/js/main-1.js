var imgCArry;
var detailhidden = true;
var cathidden = true;
var newshidden = true;
var loginhidden = true;
var buchhidden = true;
var bewerthidden = true;
var sending = false;
var isTouch = false;

var mapexists = false;
var notrack = false;


function isTouchDevice(){
  return (typeof(window.ontouchstart) != 'undefined') ? true : false;
}




function scrollbarWidth() {
	
    var div = $('<div style="width:50px;height:50px;overflow:hidden;position:absolute;top:-200px;left:-200px;"><div style="height:100px;">');
    // Append our div, do our calculation and then remove it
    $('body').append(div);
    var w1 = $('div', div).innerWidth();
    div.css('overflow-y', 'scroll');
    var w2 = $('div', div).innerWidth();
    $(div).remove();
    return (w1 - w2);
}



function bindHighlights(){
	$('.highlights-btn').bind('click', function(event) {
		if($(this).parent().find('.detail').css('display') == 'none'){
			$(this).parent().find('.detail').show('fast');
			$(this).parent().find('.open').html("<img src='/files/img/close.png'/>");
		}else{
			$(this).parent().find('.detail').hide('fast');
			$(this).parent().find('.open').html("<img src='/files/img/arrow_right.png'/>");
		}
				
		
	});
	$('.kundensection-btn').bind('click', function(event) {
		if($(this).parent().find('.detail').css('display') == 'none'){
			$(this).parent().find('.detail').show('fast');
			$(this).parent().find('.open').html("<img src='/files/img/close.png'/>");
			$(this).parent().find('h2').addClass('active');
		}else{
			$(this).parent().find('.detail').hide('fast');
			$(this).parent().find('.open').html("<img src='/files/img/arrow_right.png'/>");
			$(this).parent().find('h2').removeClass('active');
		}
				
		
	});
}


	



function showhideBuch(){
	
		if(buchhidden){
			
 			$('#detailtext .buchungsanfrage').show("fast");
			$('#detailtext .buchungsbtn').html($('#detailtext .buchungsbtn').attr('data-o') +"<img src='/files/img/close.png'/>");
			
			
			
			buchhidden = false;
		}else{
			$('#detailtext .buchungsanfrage').hide("fast");
			$('#detailtext .buchungsbtn').html($('#detailtext .buchungsbtn').attr('data-c') +"<img src='/files/img/arrow_right.png'/>");
			$('#detailtext .buchung').css('cursor','pointer');
			
			buchhidden = true;
			setTimeout( "bindBuch()",100 );
		}
	
}

function bindBuch(){
	$('#detailtext .buchung').bind('click', function(event) {
				//$(this).find('.info').css("display","none");
				if(buchhidden){
					$(this).unbind(event);
					showhideBuch();
					$('#detailtext .buchung').css('cursor','default');
					
				
				}
		
	});
}

function setloadMore(){
	$('.loadmore').bind('click', function(e) {
		e.preventDefault();
		$('.loadmoreDiv').html('<div class="error"><img src="/files/img/loader2.gif"/> searching...</div>');
		$.get($(this).attr('href'), function(data){ 
		  $('.loadmoreDiv').remove();
		  $(data).appendTo("#searchResultDiv");
		  $(".google_iframe").fancybox({
								'width'             : '75%',
								'height'            : '75%',
								'autoScale'         : false,
								'transitionIn'      : 'fade',
								'transitionOut'     : 'fade',
								'type'              : 'iframe',
								'overlayColor':'#000'
							});
			setloadMore();
		});
		
		
	});
	
}

function closeSection(section){
	if($(section).find('.sectionContent').css('display') != 'none'){
		$(section).find('.sectionContent').hide("fast");
		$(section).find('.sectionbtn').html($(section).find('.sectionbtn').attr('data-c') +"<img src='/files/img/arrow_right.png'/>");
		$(section).find('.sectionbtn').unbind('click');
		bindSection(section);
	}
}


function bindSection(section){
	$(section).bind('click', function(event) {
				//$(this).find('.info').css("display","none");
				
				 $('#detailtext .section').each(function () {
					 if(this != section)
					 	closeSection(this);
					 
				 });
				
				if($(this).find('.sectionContent').css('display') == 'none'){
					
					$(this).unbind(event);
					
					
					$(this).find('.sectionbtn').bind('click', function() {
						//$(this).find('.info').css("display","none");
						$(this).parent().find('.sectionContent').hide("fast");
						$(this).html($(this).attr('data-c') +"<img src='/files/img/arrow_right.png'/>");
						$(this).unbind(event);
						bindSection($(this).parent());
				
					});
					
					
					
					$(this).find('.sectionContent').show("fast");
					$(this).find('.sectionbtn').html($(this).find('.sectionbtn').attr('data-o') +"<img src='/files/img/close.png'/>");
					
					
					if($(this).hasClass('map'))
					{
						
						 
						  
						 $(this).find('.hotelmap').each(function () {
							 
							 if(mapexists){
								 google.maps.event.trigger(map, 'resize');
								 console.log('exists');
							 }else{
								 mapexists=true;
							 var locations = { luzern: [parseFloat($(this).attr('data-lat')), parseFloat($(this).attr('data-long'))]},
						  main = 'luzern',
						  defaultZoom = 13;
						  var mapOptions = {
								zoom: defaultZoom,
								disableDefaultUI: false,
								scrollwheel: false,
					
								center: new google.maps.LatLng(parseFloat($(this).attr('data-lat')), parseFloat($(this).attr('data-long')))
								
							  },
							  map = new google.maps.Map(this, mapOptions),
							  ll_locations = {},
							  markers = {};
						
						  $.each(locations, function (key, coords) {
							ll_locations[key] = new google.maps.LatLng(coords[0], coords[1]);
						  });
					
						  $.each(ll_locations, function (key, pos) {
							markers[key] = new google.maps.Marker({position: pos, map: map,icon: '/files/img/markers.png'});
							//console.log(pos);
						  });
						  
						  map.mapTypes.set('gray_map',
							new google.maps.StyledMapType([{ stylers: [ { saturation: 0 } ]}],
														  {name: "Gray Map"}));
						  map.setMapTypeId('gray_map');
							 }
						  
							
						 });

						
						
						
						
					}
					
					
					
				}else{
					
				}
		
	});
}



function showhideBewert(){
	if(bewerthidden){
 			$('#detailtext .bewertungen').show("fast");
			$('#detailtext .bewertungbtn').html($('#detailtext .bewertungbtn').attr('data-o') +"<img src='/files/img/close.png'/>");
			
			bewerthidden = false;
		}else{
			$('#detailtext .bewertungen').hide("fast");
			$('#detailtext .bewertungbtn').html($('#detailtext .bewertungbtn').attr('data-c') +"<img src='/files/img/arrow_right.png'/>");
			$('#detailtext .bewertung').css('cursor','pointer');	
			bewerthidden = true;
			setTimeout( "bindBewert()",100 );
		}
	
	
}

function bindBewert(){
	$('#detailtext .bewertung').bind('click', function(event) {
				//$(this).find('.info').css("display","none");
				if(bewerthidden){
					$(this).unbind(event);
					showhideBewert();
					$('#detailtext .bewertung').css('cursor','default');
					
				
				}
		
	});
}


function loadNextImage(image,num){
	
	
	image.bind("load", function() {
		console.log('loaded '+image.attr('data-original'));
		$('#imagegrid').removeClass("loading");
		image.css('display','none');
		image.parent().parent().css('background','url('+ image.attr('data-original') +')');
		image.parent().parent().css('background-position','center');
		image.parent().parent().css('background-size','cover');
		image.parent().parent().css('display','block');
				
	});
	image.bind("error", function() {
		
		/*if(image.attr("src").indexOf("_z.jpg") != -1){
			var newurl =image.attr("src").replace("_z.jpg","_y.jpg");
			image.parent().attr("href").replace("_z.jpg","_y.jpg");
			console.log(image.parent());
			image.attr("src",newurl);
			image.unbind("load");
			image.bind("load", function() {
				console.log('loaded '+newurl);
				$('#imagegrid').removeClass("loading");
				image.css('display','none');
				image.parent().parent().css('background','url('+ newurl +')');
				image.parent().parent().css('background-position','center');
				image.parent().parent().css('background-size','cover');
				image.parent().parent().css('display','block');
						
			});
			
			
		}else{*/
			image.parent().parent().remove();
			layoutgrid();
		//}
				
	});
	image.css('display','none');
	image.attr("src", image.attr('data-original'));
	
}

function showLogin(){
    $('#loginDiv').fadeIn('fast');
    			$('.login').addClass('active');
    			if($(window).height()<640)
    			$('#navigation').height(640);
    			loginhidden = false;
    			$('#newsletterDiv').css('display','none');
    			$('.newsletter').removeClass('active');
    			newshidden = true;
}

function showCat(){
	if(cathidden){
		TweenLite.to($('#categories'), 0.3, {css:{width:'220px'}, ease:Power2.easeOut});
		cathidden = false;	
		}
}

function hideCat(){
	if(!cathidden){
		TweenLite.to($('#categories'), 0.3, {css:{width:'0px'}, ease:Power2.easeOut});	
		cathidden = true;
		}
}

$(document).ready(function() {
	
	isTouch = isTouchDevice();
	
	bindHighlights();


    $('.highlight .detail.fade').show('fast');


	$('.AllowOnlyNumericInputs').ForceNumericOnly();
	
	//$('input:not(:submit,:checkbox)').bind('focus blur', change);
	//$('textarea').bind('focus blur', change);
	$(":input[placeholder]").placeholder();
	$('#imagegrid').width($(window).innerWidth()-298-scrollbarWidth());
	$('nav.typmenu').width($(window).innerWidth()-298-scrollbarWidth());
	$('#content').width($(window).innerWidth()-252-scrollbarWidth());
	 $('#imagegrid').addClass("loading");
	if ($("#imagegrid").length > 0){

		$("nav.typmenu").sticky({topSpacing:0});
	 	//$('#imagegrid').waitForImages(function() {
			if(gridtype=="detail2"){
				 imagestoload = new Array();
				
				$("#imagegrid img").each(function(index, element) {
                   imagestoload.push($(this));
					layoutgrid();
				   loadNextImage($(this));
                });
				
				
				
			}else{
				$("#imagegrid img").lazyload({
				 effect       : "fadeIn",
					threshold:200
			 	});
				$('#imagegrid').removeClass("loading");
          		layoutgrid();
			
				$('#imagegrid').width($(window).width()-298);
				layoutgrid();
			}
		 	
			
      	//});
	}else{
		$('#imagegrid').addClass("loading");
		$('#content').waitForImages(function() {
		 $('#content').removeClass("loading");
		 $('#content').width($(window).innerWidth()-252);
          if($(window).height()>$('#content .contenttext').height()+260)
			$('#content').height($(window).height());
			else
			$('#content').height($('#content .contenttext').height()+260);
		TweenLite.to($('#content .contenttext'), 0.5, {delay:0.5,css:{opacity:1}, ease:Power2.easeOut});	
      });
		
	}

	
	$('#imagegrid .item').bind('mouseenter', function() {
		$(this).find('.info').css("display","block");
		$(this).find('.zoom').css("display","block");
		if(gridtype=="detail" ||gridtype=="detail2"){
		TweenLite.to($(this).find('.zoom'), 0.3, {css:{opacity:1,left:"0%",top:"0%"}, ease:Power2.easeOut});
		}else{
            if($(this).hasClass('denied')){
                TweenLite.to($(this).find('.info'), 0.0, {css:{left:"0%",top:"0%"}, ease:Power2.easeOut});
                TweenLite.to($(this).find('.info'), 0.3, {css:{opacity:1}, ease:Power2.easeOut});
            }else
 		        TweenLite.to($(this).find('.info'), 0.3, {css:{opacity:1,left:"0%",top:"0%"}, ease:Power2.easeOut});
        }
	});
	$('#imagegrid .item').bind('mouseleave', function() {
		//$(this).find('.info').css("display","none");
		if(gridtype=="detail" ||gridtype=="detail2"){
		TweenLite.to($(this).find('.zoom'), 0.2, {css:{opacity:0,left:"100%",top:"100%"}, ease:Power2.easeIn});
        }else{

            if($(this).hasClass('denied')){

                          TweenLite.to($(this).find('.info'), 0.2, {css:{opacity:0}, ease:Power2.easeOut});
                       }else
                        TweenLite.to($(this).find('.info'), 0.2, {css:{opacity:0,left:"100%",top:"100%"}, ease:Power2.easeIn});

        }

	});
	
	
	
	$('#detailtext .button').bind('click', function() {
		//$(this).find('.info').css("display","none");
		if(detailhidden){
 			$('#detailtext .hiddendetail').show("fast");
			$('#detailtext .buttonO').html($('#detailtext .buttonO').attr('data-o') +"<img src='/files/img/arrow_up.png'/>");
			detailhidden = false;
		}else{
			$('#detailtext .hiddendetail').hide("fast");
			$('#detailtext .buttonO').html($('#detailtext .buttonO').attr('data-c') +"<img src='/files/img/arrow_down.png'/>");
		
			detailhidden = true;
		}

	});
	
	
	$('#detailtext .buchungsbtn').bind('click', function() {
		//$(this).find('.info').css("display","none");
		if(!buchhidden)
		showhideBuch();

	});
	
	bindBuch();
	
	$('#detailtext .bewertungbtn').bind('click', function() {
		//$(this).find('.info').css("display","none");
		if(!bewerthidden)
		showhideBewert();

	});
	
	bindBewert();
	

	
	$('#detailtext .section').each(function(index){
		bindSection(this);
	});
	
	
	$('#changeSearch').bind('click', function() {
		$("#searchFormDiv").show('fast');
		$("#searchInfoDiv").hide('fast');
		
	});
	
	$('#categoriesBtn img.cat').bind('click', function() {
		if(cathidden){
		TweenLite.to($('#categories'), 0.3, {css:{width:'220px'}, ease:Power2.easeOut});
		cathidden = false;	
		}else{
		TweenLite.to($('#categories'), 0.3, {css:{width:'0px'}, ease:Power2.easeOut});	
		cathidden = true;
		}
	});
	
	if(!isTouch){
	//$('#categoriesBtn img.cat').bind('mouseenter', function() {
		//showCat();
	//});
	}
	
	$('#content').bind('mouseenter', function() {
		hideCat();
	});
	
	$('#detailtext').bind('mouseenter', function() {
		hideCat();
	});
	
	$('#imagegrid').bind('mouseenter', function() {
		hideCat();
	});
	
	
	
	var input = document.getElementById('fpassword');

	if(input){
	if(input.addEventListener) {
		
		input.addEventListener('focus', function(){
			this.type = 'password'
			this.value = '';
			this.className = 'text';
		}, false);
	} else if(input.attachEvent) {
		
		input.attachEvent('onfocus', function(){
			this.type = 'password'
			this.value = '';
			this.className = 'text';
		});
	}
	}
	var input = document.getElementById('fpassword1');
	if(input){
	if(input.addEventListener) {
		
		input.addEventListener('focus', function(){
			this.type = 'password'
			this.value = '';
			this.className = 'text';
		}, false);
	} else if(input.attachEvent) {
		
		input.attachEvent('onfocus', function(){
			this.type = 'password'
			this.value = '';
			this.className = 'text';
		});
	}
	}
	
	var input = document.getElementById('fpassword2');
	if(input){
	if(input.addEventListener) {
		
		input.addEventListener('focus', function(){
			this.type = 'password'
			this.value = '';
			this.className = 'text';
		}, false);
	} else if(input.attachEvent) {
		
		input.attachEvent('onfocus', function(){
			this.type = 'password'
			this.value = '';
			this.className = 'text';
		});
	}
	}
	$('#newsletterBtn').bind('click', function(e) {
		e.preventDefault();
		if(newshidden){
			$('#newsletterDiv').fadeIn('fast');
			$('.newsletter').addClass('active');
			if($(window).height()<640)
			$('#navigation').height(640);
			newshidden = false;	
			
			$('#loginDiv').css('display','none');
			$('.login').removeClass('active');
			loginhidden = true;	
		}else{
			$('#navigation').css('height','100%');
			$('#newsletterDiv').fadeOut('fast');
			$('.newsletter').removeClass('active');
			newshidden = true;	
		}
	});
	
	$('#loginBtn').bind('click', function(e) {
		e.preventDefault();
		if(loginhidden){
			$('#loginDiv').fadeIn('fast');
			$('.login').addClass('active');
			if($(window).height()<640)
			$('#navigation').height(640);
			loginhidden = false;
			$('#newsletterDiv').css('display','none');
			$('.newsletter').removeClass('active');
			newshidden = true;		
		}else{
			$('#navigation').css('height','100%');
			$('#loginDiv').fadeOut('fast');
			$('.login').removeClass('active');
			loginhidden = true;	
		}
	});
	$('.showbewertungsform').bind('click', function(e) {
		e.preventDefault();
		if(!$(this).hasClass('off')){
			$(this).parent().parent().next().show('fast');
			$(this).parent().parent().next().find('.bewertungsformDiv').show('fast');
		}
		
	});
	
	$('.close-btn').bind('click', function(e) {
		
		$(this).parent().parent().parent().find('.bewertungsformDiv').hide('fast',function(){$(this).parent().parent().css('display','none');});
		
	});
	
	$('.starrating').each(function(index, element) {
		console.log($(this));
        $(this).raty({
		  target    : '#starfield'+index,
		  targetType: 'number',
		  starOff: '/files/img/star-off.png',
		  starOn : '/files/img/star-on.png',
		  targetKeep: true,
		  score:$(this).attr('data-num')
		});
    });
	
	
	if( $( ".datepicker" ).length>0)
    $( ".datepicker" ).datepicker({ dateFormat: 'dd.mm.yy' });
	
	$( "#arrival" ).change(function() {
		
		
	  if($( "#depature").val() == "" || $( "#depature" ).datepicker( "getDate" )<$( "#arrival" ).datepicker( "getDate" )){
		  	var myDate = $( "#arrival").datepicker( "getDate" );
			console.log(myDate);
			myDate.setTime(myDate.getTime() +  (1 * 24 * 60 * 60 * 1000));
		  
	  		$( "#depature").datepicker( "setDate", myDate);
	  }
	});
	
	// contactform Form --------------------------------------------------------------------------------------------------------------------------------------------
	
	$('#contactform').ajaxForm({
		data: { date: JSON.stringify(new Date())},
		target: '#gesendet',
			beforeSubmit: function() {
				
				
				$('#gesendet').addClass('visible');
				$('#gesendet').html("<img src='/files/img/loader3.gif' />");
				if(sending)
					return false;
					else
				sending = true;
			},
				success: function() {
					if($('#gesendet').html() == danketext){
					
						
					}
					sending = false;
					$('#gesendet').slideDown();
				}
	});
	
	// newsletterform Form --------------------------------------------------------------------------------------------------------------------------------------------
	
	$('#newsletterform').ajaxForm({
		data: { date: JSON.stringify(new Date())},
		target: '#gesendet3',
			beforeSubmit: function() {
				
				
				$('#gesendet3').addClass('visible');
				$('#gesendet3').html("<img src='/files/img/loader3.gif' />");
				if(sending)
					return false;
					else
				sending = true;
			},
				success: function() {
					if($('#gesendet3').html() == 'ok'){
						$('#newsletterDiv').fadeOut('fast');
						$('.newsletter').removeClass('active');
						newshidden = true;	
						alert(eingetragentext);
					
						sending = false;
						$('#gesendet3').slideDown();
						
					}
					
				}
	});
	// buchungsform Form --------------------------------------------------------------------------------------------------------------------------------------------
	
	$('#buchungsform').ajaxForm({
		data: { date: JSON.stringify(new Date())},
		target: '#gesendet',
			beforeSubmit: function() {
				
				
				$('#gesendet').addClass('visible');
				$('#gesendet').html("<img src='/files/img/loader3.gif' />");
				if(sending)
					return false;
					else
				sending = true;
			},
				success: function() {
					if($('#gesendet').html() == danketext){
						
					}
					sending = false;
					$('#gesendet').slideDown();
				}
	});
	
	
	// Registrierung Form --------------------------------------------------------------------------------------------------------------------------------------------
	
	 $('#registerform').ajaxForm({
		 		data: { date: JSON.stringify(new Date())},
				target: '#gesendet',
					beforeSubmit: function() {
						
						if(sending)
						return false;
						else{
						sending = true;
						$('#gesendet').html('<img src="/files/img/loader3.gif"/>');
						}
					},
						success: function() {
							
							sending = false;
							if($('#gesendet').html() == 'ok'){
								
								$("#registerFormDiv").css('display','none');
								$("#registerFormDivReady").show('fast');
								
								
							}
							
						}
			});
	
	// User Form --------------------------------------------------------------------------------------------------------------------------------------------
	
	 $('#userform').ajaxForm({
				target: '#gesendet',
					beforeSubmit: function() {
						if(sending)
						return false;
						else{
						sending = true;
							$('#gesendet').html('<img src="/files/img/loader3.gif"/>');
						}
					},
						success: function() {
							
							sending = false;
							if($('#gesendet').html() == 'ok'){
								
								
							}
							
						}
			});
			
	// Delete User Form 2--------------------------------------------------------------------------------------------------------------------------------------------
	
	$("#deleteprofile").bind('click', function(e) {
		e.preventDefault();
        if (confirm('Are you sure to delete?')) {
            $('#userform2').submit();
        }
    });
	
	
	 $('#userform2').ajaxForm({
				target: '#gesendet',
					beforeSubmit: function() {
						
						
							if(sending)
							return false;
							else{
								sending = true;
								$('#gesendet').html('<img src="/files/img/loader3.gif"/>');
							}

						
						
					},
						success: function() {
							
							sending = false;
							
							if($('#gesendet').html() == 'ok'){
								document.location.href = "/de/goodbye.html?FORM[LOGOUT]=1";
								
							}
							
						}
			});
			
			
	// Bewertungs Form 2--------------------------------------------------------------------------------------------------------------------------------------------
	
	 $('.bewertungsform').each(function(index, element) {
        console.log($(this));
    	$(this).ajaxForm({
				target: '#gesendet'+index,
					beforeSubmit: function() {
						
						if(sending)
						return false;
						else{
							$('#gesendet'+index).html('<img src="/files/img/loader3.gif"/>');
							sending = true;
						}
						
						
						
					},
						success: function() {
							
							sending = false;
							
							if($('#gesendet'+index).html() == 'ok'){
								
								
							}
							
						}
			});
		});
	
	
	// Search Form --------------------------------------------------------------------------------------------------------------------------------------------
	
	 $('#searchform').ajaxForm({
		 		data: { date: JSON.stringify(new Date())},
				target: '#searchResultDiv',
					beforeSubmit: function() {
						
						
						if(sending)
						return false;
						else
						sending = true;
						$('#searchFormDiv').css('margin-bottom','20px');
						$('#searchResultDiv').prepend('<div class="error"><img src="/files/img/loader2.gif"/> searching...</div>');
						$('#searchResultDiv').show('fast');
						
						
					},
						success: function() {
							
							var string = "";
							var adults = 0;
							var children = 0;
							var zimmer2 = 0;
							
							for( i =1;i<9;i++){
								if($('#adults'+i).length > 0){
									adults += parseInt($('#adults'+i).val());
									zimmer2++;
								}
								if($('#children'+i).length > 0)
									children += parseInt($('#children'+i).val());	
							}
							
							string = zimmer2+' '+zimmer+'<span class="orange"> / </span>'+adults+' '+erwachsene;
							if(children > 0)
							 string += '<span class="orange"> / </span>'+children+' '+kinder;
							 
							string2 = zimmer2+' '+zimmer+' / '+adults+' '+erwachsene;
							if(children > 0)
							 string2 += ' / '+children+' '+kinder;
							 
							if(!notrack){
								_gaq.push(['_trackEvent', 'Search', $('#destination').val(), $('#arrival').val()+' / '+$('#depature').val()+' / '+string2]);
								
							}
							
							notrack = false;
							
							
							$('#currentSearch').html('<p>'+$('#destination').val()+'<span class="orange"> / </span> Ab ' +$('#arrival').val()+' bis '+$('#depature').val()+'<span class="orange"> /<br> </span>'+string+'</p>');	
							setloadMore();
							
							if($('#searchResultDiv').find('.error').length<1){
								$('#searchFormDiv').css('margin-bottom','20px');
								$('#searchFormDiv').hide('fast');
								$('#searchInfoDiv').show('fast');
							}
							$('#searchResultDiv').show('fast');
							
							$('#content').width($(window).innerWidth()-252-scrollbarWidth());
							$('#content').width($(window).innerWidth()-252);
							sending = false;
						
							$(".google_iframe").fancybox({
								'width'             : '75%',
								'height'            : '75%',
								'autoScale'         : false,
								'transitionIn'      : 'fade',
								'transitionOut'     : 'fade',
								'type'              : 'iframe',
								'overlayColor':'#000'
							});
			
							
							
						}
			});
	
			
			if($('#searchResultDiv').hasClass('cached')){
				
					$('#searchFormDiv').css('display','none');
					$('#cachedSearch').val('1');
					notrack = true;
					$('#searchform').submit();
					$('#cachedSearch').val('0');
				}
			
	// Search Zimmer --------------------------------------------------------------------------------------------------------------------------------------------
	
	 $('#zimmerform').ajaxForm({
		 		data: { date: JSON.stringify(new Date())},
				target: '#searchResultDiv',
					beforeSubmit: function() {
						
						
						if(sending)
						return false;
						else
						sending = true;
						$('#searchFormDiv').css('margin-bottom','20px');
						$('#searchResultDiv').prepend('<div class="error2 small"><img src="/files/img/loader2.gif"/> searching...</div>');
						$('#searchResultDiv').show('fast');
						
						
					},
						success: function() {
							
							
							var string = "";
							var adults = 0;
							var children = 0;
							var zimmer2 = 0;
							
							for( i =1;i<9;i++){
								if($('#adults'+i).length > 0){
									adults += parseInt($('#adults'+i).val());
									zimmer2++;
								}
								if($('#children'+i).length > 0)
									children += parseInt($('#children'+i).val());	
							}
							
							string = zimmer2+' '+zimmer+'<span class="orange"> / </span>'+adults+' '+erwachsene;
							if(children > 0)
							 string += '<span class="orange"> / </span>'+children+' '+kinder;
							
							$('#currentSearch').html('<p>Ab ' +$('#arrival').val()+' bis '+$('#depature').val()+'<span class="orange"> /<br></span>'+string+'</p>');
							if($('#searchResultDiv').find('.error').length<1){
								$('#searchFormDiv').css('margin-bottom','20px');
								$('#searchFormDiv').hide('fast');
								$('#searchInfoDiv').show('fast');
							}
							$('.zimmerinfo').bind('click', function(event) {
								if($(this).parent().parent().find('.zimmerinfoDetail').css('display') == "none")
								$(this).parent().parent().find('.zimmerinfoDetail').show('fast');
								else
								$(this).parent().parent().find('.zimmerinfoDetail').hide('fast');
							});
							
							$('#searchResultDiv').show('fast');
							
							sending = false;
							
							
						}
			});
			
			if (typeof(gridtype) != "undefined"){
			if(gridtype=="detail2" || gridtype=="detail"){
				if($('#arrival').val() != "" && $('#depature').val() != "" && $('#adults1').val() != ""){
					$('#searchFormDiv').css('display','none');
					$('#zimmerform').submit();
				}
				
			}
			}
			
			
			
			$('#roomPlus').bind('click', function(e) {
				if(roomNum<8){
				roomNum++;
				//newRoom = '<div class="roomPlus"><div class="abstand">'+zimmer+' '+roomNum+':</div><input type="number" min="0" max="4" data-length="1" data-max="4" class="AllowOnlyNumericInputs form-input person" name="adults'+roomNum+'" id="adults'+roomNum+'" placeholder="'+erwachsene+' 1-4" /> <input type="number" min="0" max="3" data-length="1" data-max="3" class="AllowOnlyNumericInputs form-input person2" name="children'+roomNum+'" id="children'+roomNum+'" placeholder="'+kinder+' 0-3" /><img class="roomRemove" src="/files/img/close.png"/>';
				
				
				newRoom = '<div class="roomPlus"><div class="abstand">'+zimmer+' '+roomNum+'</div> <div style="float:left; margin-top:10px;"><img src="/files/img/person2.png" /></div><div style="width:130px; margin-top:10px;margin-bottom:10px; margin-right:10px; float:left;"><select class="selectbox small" id="adults'+roomNum+'" name="adults'+roomNum+'" >';
				
				newRoom +=  '<option value="1" >'+erwachsene+' 1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option>    ';
				newRoom += '</select></div>';
						
                 newRoom +=      '<div style="float:left; margin-top:10px;"><img src="/files/img/person2s.png" /></div><div style="width:100px; margin-top:10px; margin-bottom:10px;float:left;"><select class="selectbox small person2" id="children'+roomNum+'" name="children'+roomNum+'" ><option value="0" >'+kinder+' 0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option> ';
                         			
                                   
                                   
                   newRoom +=  '</select></div><img style="margin-top:15px; margin-left:10px;" class="roomRemove" src="/files/img/close.png"/><div class="clearfix"></div>';
				
				
				
				
				newRoom += '<div class="abstand up age" id="children'+roomNum+'div">'+alter+':</div><div class="ageDiv age"  id="children'+roomNum+'age1div"><select class="selectbox small" id="children'+roomNum+'age1" name="children'+roomNum+'age1" ><option value="">'+kind+' 1</option><option value="0">< 1</option>';
                             
										for($z = 2 ; $z<18; $z++){
											newRoom +=  '<option value="'+$z+'">'+$z+'</option>';
											
											
										}
									
									
									
                                   
                                   
                        newRoom +='</select></div><div class="ageDiv age"  id="children'+roomNum+'age2div"><select class="selectbox small" id="children'+roomNum+'age2" name="children'+roomNum+'age2" ><option value="">'+kind+' 2</option><option value="0">< 1</option>';
                                  
										for($z = 2 ; $z<18; $z++){
											newRoom +=  '<option value="'+$z+'">'+$z+'</option>';
											
											
										}
									
									
								
                                   
                                   
                        newRoom +='</select></div><div class="ageDiv age"  id="children'+roomNum+'age3div"><select class="selectbox small" id="children'+roomNum+'age3" name="children'+roomNum+'age3" ><option value="">'+kind+' 3</option><option value="0">< 1</option>';
                                  
										for($z = 2 ; $z<18; $z++){
											newRoom +=  '<option value="'+$z+'">'+$z+'</option>';
											
											
										}
                                   
                        newRoom +='</select></div><div class="clearfix"></div></div>';
				//alert(newRoom);
			  $('#zimmerDiv').append(newRoom);
			  $(".roomRemove").unbind('click');
			  $('.AllowOnlyNumericInputs').ForceNumericOnly();
			 $(".selectbox").selectbox({onChange: function (val, inst) {
				
				//alert($(this).attr('id'));
				if($(this).hasClass('person2')){
				for(j=0;j<3;j++){
						
						$("#"+$(this).attr('id')+"div").css('display','none');
						$("#"+$(this).attr('id')+"age"+(j+1)+"div").css('display','none');
						
					}
					for(j=0;j<val;j++){
						
						$("#"+$(this).attr('id')+"div").css('display','block');
						$("#"+$(this).attr('id')+"age"+(j+1)+"div").css('display','block');
						
					}
				}
			}});
			   
			  $('.roomRemove').bind('click', function(e) {
					
			 	 $(this).parent().last('.roomPlus').remove();
				 roomNum--;
						
				});
			  
				}
			});
			
			
			
			
			 $(".roomRemove").unbind('click');
			  $('.roomRemove').bind('click', function(e) {
					
			 	 $(this).parent().last('.roomPlus').remove();
				  roomNum--;
						
				});
				
				
				
				
		
			$(".selectbox").selectbox({onChange: function (val, inst) {
				
				//alert($(this).attr('id'));
				if($(this).hasClass('person2')){
				for(j=0;j<3;j++){
						
						$("#"+$(this).attr('id')+"div").css('display','none');
						$("#"+$(this).attr('id')+"age"+(j+1)+"div").css('display','none');
						
					}
					for(j=0;j<val;j++){
						
						$("#"+$(this).attr('id')+"div").css('display','block');
						$("#"+$(this).attr('id')+"age"+(j+1)+"div").css('display','block');
						
					}
				}
			}});
			
			 $('input').iCheck({
				checkboxClass: 'icheckbox_square-orange',
				radioClass: 'iradio_square-orange',
				increaseArea: '20%' // optional
			  });


    $('.biginfotext h1').css('font-size', ($('.biginfotext').width() / 800 * 35) + 'px');
    $('.biginfotext h1').css('line-height', ($('.biginfotext').width() / 800 * 35) + 'px');
    $('.biginfotext p').css('font-size', ($('.biginfotext').width() / 800 * 14) + 'px');
    $('.biginfotext p').css('line-height', ($('.biginfotext').width() / 800 * 18) + 'px');
    $('.biginfotext a').css('font-size', ($('.biginfotext').width() / 800 * 22) + 'px');
    $('.biginfotext a').css('line-height', ($('.biginfotext').width() / 800 * 22) + 'px');

    $('.smallinfo h1').css('font-size', ($('.smallinfo').width() / 700 * 24) + 'px');
    $('.smallinfo h1').css('line-height', ($('.smallinfo').width() / 700 * 24) + 'px');
    $('.smallinfo h1 span').css('font-size', ($('.smallinfo').width() / 700 * 32) + 'px');
    $('.smallinfo h1 span').css('line-height', ($('.smallinfo').width() / 700 * 32) + 'px');
    $('.smallinfo p').css('font-size', ($('.smallinfo').width() / 700 * 14) + 'px');
    $('.smallinfo p').css('line-height', ($('.smallinfo').width() / 700 * 15) + 'px');

    $('.eventtip h2').css('font-size', ($('.biginfotext').width() / 800 * 24) + 'px');
            $('.eventtip h2').css('line-height', ($('.biginfotext').width() / 800 * 24) + 'px');
            $('.eventtip p').css('font-size', ($('.biginfotext').width() / 800 * 14) + 'px');
            $('.eventtip p').css('line-height', ($('.biginfotext').width() / 800 * 15) + 'px');
});


$(window).resize(function() {

	if ($("#imagegrid").length > 0){


		$('#imagegrid').width($(window).width()-298);
		$('nav.typmenu').width($(window).innerWidth()-298);
		layoutgrid();
		//console.log('test');

	}else{
		$('#content').width($(window).innerWidth()-252-scrollbarWidth());
          if($(window).height()>$('#content .contenttext').height()+260)
			$('#content').height($(window).height());
			//else
			//$('#content').height($('#content .contenttext').height()+260);
			
	}

    $('.biginfotext h1').css('font-size', ($('.biginfotext').width() / 800 * 35) + 'px');
   $('.biginfotext h1').css('line-height', ($('.biginfotext').width() / 800 * 35) + 'px');
   $('.biginfotext p').css('font-size', ($('.biginfotext').width() / 800 * 14) + 'px');
   $('.biginfotext p').css('line-height', ($('.biginfotext').width() / 800 * 18) + 'px');
   $('.biginfotext a').css('font-size', ($('.biginfotext').width() / 800 * 22) + 'px');
   $('.biginfotext a').css('line-height', ($('.biginfotext').width() / 800 * 22) + 'px');

   $('.smallinfo h1').css('font-size', ($('.smallinfo').width() / 700 * 24) + 'px');
   $('.smallinfo h1').css('line-height', ($('.smallinfo').width() / 700 * 24) + 'px');
   $('.smallinfo h1 span').css('font-size', ($('.smallinfo').width() / 700 * 32) + 'px');
   $('.smallinfo h1 span').css('line-height', ($('.smallinfo').width() / 700 * 32) + 'px');
   $('.smallinfo p').css('font-size', ($('.smallinfo').width() / 700 * 14) + 'px');
   $('.smallinfo p').css('line-height', ($('.smallinfo').width() / 700 * 15) + 'px');

   $('.eventtip h2').css('font-size', ($('.biginfotext').width() / 800 * 24) + 'px');
       $('.eventtip h2').css('line-height', ($('.biginfotext').width() / 800 * 24) + 'px');
       $('.eventtip p').css('font-size', ($('.biginfotext').width() / 800 * 14) + 'px');
       $('.eventtip p').css('line-height', ($('.biginfotext').width() / 800 * 15) + 'px');

	
});



function layoutgrid(){


	
	
	var imagegridwidth=$('#imagegrid').width();
	var rowcount = Math.round(imagegridwidth/400);
	if(rowcount>3&&(gridtype=="detail" ||gridtype=="detail2"))
		rowcount=3;
	var rowwidth = Math.round(imagegridwidth/rowcount);
	var fact = rowwidth/400;
	var fact2 = fact;
	
	var i=0;
	var r=0;
	var l=0;
	var posX=0;
	var posY=0;
    var spe = 1;


    if (gridtype == "detail") {
        if (rowcount == 1) {
            var imgCord = "1;0;0;400;250||2;0;250;400;500||2;0;0;400;250";
            imgCArry = imgCord.split("||");
            spe = 2;
        } else if (rowcount == 2) {
            var imgCord = "1;0;0;800;500||2;0;500;400;500||2;400;500;400;250||2;400;750;400;250||2;0;0;400;250";
            imgCArry = imgCord.split("||");
            spe = 4;
        } else if (rowcount == 3) {
            var imgCord = "1;0;0;800;500||2;800;0;400;500||2;0;0;400;250";
            imgCArry = imgCord.split("||");
            spe = 2;
        } else {
            var imgCord = "1;0;0;800;500||2;800;0;400;500||3;1200;0;400;250||3;1200;250;400;250||4;0;0;400;250";
            imgCArry = imgCord.split("||");
            spe = 4;
        }


    }
    if (gridtype == "detail2") {
        if (rowcount == 1) {
            var imgCord = "1;0;0;400;250||2;0;250;400;500||2;0;0;400;250";
            imgCArry = imgCord.split("||");
            spe = 2;
        } else if (rowcount == 2) {
            var imgCord = "1;0;0;800;500||2;0;500;400;500||2;400;500;400;250||2;400;750;400;250||2;0;0;400;250";
            imgCArry = imgCord.split("||");
            spe = 4;
        } else if (rowcount == 3) {
            var imgCord = "1;0;0;800;500||2;800;0;400;500||2;0;0;400;250";
            imgCArry = imgCord.split("||");
            spe = 2;
        } else {
            var imgCord = "1;0;0;800;500||2;800;0;400;500||3;1200;0;400;250||3;1200;250;400;250||4;0;0;400;250";
            imgCArry = imgCord.split("||");
            spe = 4;
        }


    } else if (gridtype == "overview") {
        if (rowcount == 1) {
            var imgCord = "1;0;0;400;250";
            imgCArry = imgCord.split("||");
            spe = 0;
        } else if (rowcount == 2) {
            var imgCord = "1;0;0;800;500||2;0;0;400;250";
            imgCArry = imgCord.split("||");
            spe = 1;
        } else if (rowcount == 3) {
            var imgCord = "1;0;0;800;500||2;800;0;400;250||3;800;250;400;250||2;0;0;400;250";
            imgCArry = imgCord.split("||");
            spe = 3;
        } else if (rowcount == 4) {
            var imgCord = "1;0;0;800;500||2;800;0;400;250||3;1200;0;400;250||4;800;250;400;250||3;1200;250;400;250||4;0;0;400;250";
            imgCArry = imgCord.split("||");
            spe = 5;
        } else if (rowcount == 5) {
            var imgCord = "1;0;0;800;500||2;800;0;400;250||3;1200;0;400;250||4;1600;0;400;250||4;800;250;400;250||3;1200;250;400;250||4;1600;250;400;250||4;0;0;400;250";
            imgCArry = imgCord.split("||");
            spe = 7;
        } else {
            rowcount = 6;
            var rowwidth = Math.round(imagegridwidth / rowcount);
            var imgCord = "1;0;0;800;500||2;800;0;400;250||3;1200;0;400;250||4;1600;0;400;250||5;2000;0;400;250||6;800;250;400;250||7;1200;250;400;250||8;1600;250;400;250||9;2000;250;400;250||4;0;0;400;250";
            imgCArry = imgCord.split("||");
            spe = 9;
        }
    } else if (gridtype == "home") {
        if (rowcount == 1) {
            var imgCord = "1;0;0;400;250";
            imgCArry = imgCord.split("||");
            spe = 0;
        } else if (rowcount == 2) {
            var imgCord = "1;0;0;800;500||2;0;500;800;500||3;0;0;400;250";
            imgCArry = imgCord.split("||");
            spe = 2;
        } else if (rowcount == 3) {
            var imgCord = "1;0;0;800;500||2;0;500;800;500||2;800;0;400;250||3;800;250;400;250||3;800;500;400;250||3;800;750;400;250||2;0;0;400;250";
            imgCArry = imgCord.split("||");
            spe = 6;
        } else if (rowcount == 4) {
            var imgCord = "1;0;0;800;500||2;0;500;800;500||2;800;0;400;250||3;1200;0;400;250||4;800;250;400;250||3;1200;250;400;250||4;800;500;400;250||3;1200;500;400;250||4;800;750;400;250||3;1200;750;400;250||4;0;0;400;250";
            imgCArry = imgCord.split("||");
            spe = 10;
        } else if (rowcount == 5) {
            var imgCord = "1;0;0;800;500||2;0;500;800;500||2;800;0;400;250||3;1200;0;400;250||4;1600;0;400;250||4;800;250;400;250||3;1200;250;400;250||4;1600;250;400;250||4;800;500;400;250||3;1200;500;400;250||4;1600;500;400;250||4;800;750;400;250||3;1200;750;400;250||4;1600;750;400;250||4;0;0;400;250";
            imgCArry = imgCord.split("||");
            spe = 14;
        } else {
            rowcount = 6;
            var rowwidth = Math.round(imagegridwidth / rowcount);
            var imgCord = "1;0;0;800;500||2;0;500;800;500||2;800;0;400;250||3;1200;0;400;250||4;1600;0;400;250||5;2000;0;400;250||6;800;250;400;250||7;1200;250;400;250||8;1600;250;400;250||9;2000;250;400;250||6;800;500;400;250||7;1200;500;400;250||8;1600;500;400;250||9;2000;500;400;250||6;800;750;400;250||7;1200;750;400;250||8;1600;750;400;250||9;2000;750;400;250||4;0;0;400;250";
            imgCArry = imgCord.split("||");
            spe = 18;
        }
    }/* else if (gridtype == "home2") {
            if (rowcount == 1) {
                var imgCord = "1;0;0;400;250";
                imgCArry = imgCord.split("||");
                spe = 0;
            } else if (rowcount == 2) {
                var imgCord = "1;0;0;800;500||2;0;500;800;250||3;0;0;400;250";
                imgCArry = imgCord.split("||");
                spe = 2;
            } else if (rowcount == 3) {
                var imgCord = "1;0;0;800;500||2;0;500;800;250||2;800;0;400;250||3;800;250;400;250||3;800;500;400;250||2;0;0;400;250";
                imgCArry = imgCord.split("||");
                spe = 5;
            } else if (rowcount == 4) {
                var imgCord = "1;0;0;800;500||2;0;500;800;250||2;800;0;400;250||3;1200;0;400;250||4;800;250;400;250||3;1200;250;400;250||4;800;500;400;250||3;1200;500;400;250||4;0;0;400;250";
                imgCArry = imgCord.split("||");
                spe = 8;
            } else if (rowcount == 5) {
                var imgCord = "1;0;0;800;500||2;0;500;800;250||2;800;0;400;250||3;1200;0;400;250||4;1600;0;400;250||4;800;250;400;250||3;1200;250;400;250||4;1600;250;400;250||4;800;500;400;250||3;1200;500;400;250||4;1600;500;400;250||4;0;0;400;250";
                imgCArry = imgCord.split("||");
                spe = 11;
            } else {
                rowcount = 6;
                var rowwidth = Math.round(imagegridwidth / rowcount);
                var imgCord = "1;0;0;800;500||2;0;500;800;250||2;800;0;400;250||3;1200;0;400;250||4;1600;0;400;250||5;2000;0;400;250||6;800;250;400;250||7;1200;250;400;250||8;1600;250;400;250||9;2000;250;400;250||6;800;500;400;250||7;1200;500;400;250||8;1600;500;400;250||9;2000;500;400;250||4;0;0;400;250";
                imgCArry = imgCord.split("||");
                spe = 14;
            }
        }*/

    $('#imagegrid .item').each(function(index, element) {

		
		$(element).css("z-index",899-i);


		
	   	if(i<spe){
			var imgCA = imgCArry[i].split(";");
			if(i==0){
				if((Math.round(imgCA[3]*fact)% 2) == 0)
					fact = (Math.round(imgCA[3]*fact))/imgCA[3];
				else
					fact = (Math.round(imgCA[3]*fact)+1)/imgCA[3];
				//console.log((fact2));
				if((Math.round(imgCA[4]*fact2)% 2) == 0)
					fact2 = (Math.round(imgCA[4]*fact2))/imgCA[4];
				else
					fact2 = (Math.round(imgCA[4]*fact2)+1)/imgCA[4];
					
					//console.log("First image: "+(Math.floor(imgCA[3]*fact))+ " "+(Math.floor(imgCA[4]*fact2))+ " " + fact2 + "" +imagegridwidth);
					
				
			}
			
			
			TweenLite.to(element, 0, {css:{left:Math.floor(imgCA[1]*fact)+"px",top:Math.floor(imgCA[2]*fact2)+"px",width:Math.floor(imgCA[3]*fact)+"px",height:Math.floor(imgCA[4]*fact2)+"px"}});
			if(gridtype=="detail2"){
				
				
			}else
				TweenLite.to($(element).find("img:not(.biginfo)"), 0, {css:{width:Math.floor(imgCA[3]*fact)+"px",height:Math.floor(imgCA[4]*fact2)+"px"}});
			//console.log("First image: "+(Math.floor(imgCA[3]*fact))+ " "+(Math.floor(imgCA[4]*fact2))+ " " + fact2 + "" +imagegridwidth);
			TweenLite.to(element, 0.2, {delay:i*0.05,css:{opacity:1}, ease:Power2.easeOut});
			//console.log($(element).css("left")+" "+$(element).css("top")+" "+$(element).width()+" "+$(element).height());
			r = 0;
			if(rowcount == 1 && (gridtype=="detail" ||gridtype=="detail2"))
				l = 3;
			else if(rowcount == 2 && (gridtype=="detail" ||gridtype=="detail2"))
				l = 4;
            else if(gridtype=="home")
                l = 4;
			else
				l = 2;
		}else{
			var imgCA = imgCArry[spe].split(";");
			TweenLite.to(element, 0, {css:{left:Math.floor((imgCA[3]*r)*fact)+"px",top:Math.floor((imgCA[4]*l)*fact2)+"px",width:Math.floor(imgCA[3]*fact)+"px",height:Math.floor(imgCA[4]*fact2)+"px" }, ease:Power2.easeOut});
			if(gridtype=="detail2"){
				
				
				
				//TweenLite.to($(element).find("img"), 0, {css:{width:"100%"}, ease:Power2.easeOut});
			}else
				TweenLite.to($(element).find("img:not(.biginfo)"), 0, {css:{width:$(element).width()+"px",height:$(element).height()+"px" }, ease:Power2.easeOut});
			TweenLite.to(element, 0.2, {delay:i*0.05,css:{opacity:1}, ease:Power2.easeOut});
			r++;
			//console.log($(element).css("left")+" "+$(element).css("top")+" "+$(element).width()+" "+$(element).height());
			//console.log($(element).find("img").css("left")+" "+$(element).find("img").css("top")+" "+$(element).find("img").width()+" "+$(element).find("img").height());
		}
		
		i++;
		
		if(r==rowcount){
			l++;
			r = 0;
		}	
		
    });
	if(r==0)
	$('#imagegrid').height((l)*(250*fact2)-2);
	else
	$('#imagegrid').height((l+1)*(250*fact2)-2);
	

	

	TweenLite.to($('#detailtext'), 0.5, {delay:0.5,css:{opacity:1}, ease:Power2.easeOut});
	
}