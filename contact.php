﻿

<div id="content">
        	<div id="map" class="background"></div> 
        	<div class="contenttext kontakt">
            	<div class="close"><a href="<?=base?>contact"><img src="<?=base?>\files\img\close.png"></a></div>
            	<h1>Lifestyle hotels</h1>
              <h2 class="white">Handverlesene, einzigartige Hotels</h2>
            	<h2 class="white">Nicole Nickel</h2>
            	<p>Moosmattstrasse 6<br>
CH-6215 Beromünster/Luzern<br>
Schweiz<br>
T +41 41 930 4307<br>
F +41 41 930 4306</p>
            	<p><a href="mailto:info@lifestyle-hotels.ch">info@lifestyle-hotels.ch</a></p>
                <br>
                <form id="contactform" action="/ru/php.html?p=sendmail" method="POST">
                <input type="text" class="form-input" name="fname" id="fname" placeholder="Name, Vorname">
                <input type="text" class="form-input" name="femail" id="femail" placeholder="E-Mail">
                <textarea class="form-text" id="fmessage" name="fmessage" placeholder="Ihre Nachricht"></textarea>
                <button class="submit-btn" name="Senden" type="submit">Senden</button>
                <input type="hidden" name="cur_lang" value="0">
                <div id="gesendet"></div>
                </form>
            
            </div>      
        
        
    </div>
		<div id="categories">
        	<ul>
            	<li><a href="..\index.html">Alle</a></li>
            </ul>
        	<h3>Länder</h3>
        	<ul class="rex-navi1"><li class="rex-article-132 rex-normal"><a href="alle\afrika.html">Afrika</a><ul class="rex-navi2"><li class="rex-article-11 rex-normal"><a href="alle\afrika\suedafrika.html">Südafrika</a></li></ul></li><li class="rex-article-112 rex-normal"><a href="alle\asien.html">Asien</a></li><li class="rex-article-131 rex-normal"><a href="alle\europa.html">Europa</a><ul class="rex-navi2"><li class="rex-article-74 rex-normal"><a href="alle\europa\deutschland.html">Deutschland</a></li><li class="rex-article-88 rex-normal"><a href="alle\europa\england.html">England</a></li><li class="rex-article-68 rex-normal"><a href="alle\europa\frankreich.html">Frankreich</a></li><li class="rex-article-16 rex-normal"><a href="alle\europa\italien.html">Italien</a></li><li class="rex-article-15 rex-normal"><a href="alle\europa\oesterreich.html">Österreich</a></li><li class="rex-article-9 rex-normal"><a href="alle\europa\schweiz.html">Schweiz</a></li><li class="rex-article-77 rex-normal"><a href="alle\europa\spanien.html">Spanien</a></li><li class="rex-article-174 rex-normal"><a href="alle\europa\portugal.html">Portugal</a></li></ul></li><li class="rex-article-250 rex-normal"><a href="alle\vae.html">VAE</a><ul class="rex-navi2"><li class="rex-article-252 rex-normal"><a href="alle\vae\ras-al-khaimah.html">Ras Al Khaimah</a></li></ul></li><li class="rex-article-227 rex-normal"><a href="alle\golfplaetze.html">GOLFPLÄTZE</a><ul class="rex-navi2"><li class="rex-article-230 rex-normal"><a href="alle\golfplaetze\spanien.html">Spanien</a></li><li class="rex-article-231 rex-normal"><a href="alle\golfplaetze\portugal.html">Portugal</a></li><li class="rex-article-243 rex-normal"><a href="alle\golfplaetze\vae.html">VAE</a></li></ul></li></ul>            
            <h3>Typ</h3>
        	<ul>
            	
           
           		<li><a href="hotelkategorien\boutique-hotel.html">Boutique Hotel</a><li><a href="hotelkategorien\design-hotel.html">Design Hotel</a><li><a href="hotelkategorien\luxushotel.html">Luxushotel</a><li><a href="hotelkategorien\resort.html">Resort</a><li><a href="hotelkategorien\golf-hotel.html">Golf Hotel</a>            </li></ul>
            
             <h3>Kategorie</h3>
        	<ul>
            <li><a href="..\index-1.html?typ=hotel&amp;stars=3"><img class="star" src="<?=base?>files\img\star.png" width="13" height="12" alt="3 Stars"><img class="star" src="<?=base?>files\img\star.png" width="13" height="12" alt="3 Stars"><img class="star" src="<?=base?>files\img\star.png" width="13" height="12" alt="3 Stars"></a></li>
            	<li><a href="..\index-2.html?typ=hotel&amp;stars=4"><img class="star" src="<?=base?>files\img\star.png" width="13" height="12" alt="4 Stars"><img class="star" src="<?=base?>files\img\star.png" width="13" height="12" alt="4 Stars"><img class="star" src="<?=base?>files\img\star.png" width="13" height="12" alt="4 Stars"><img class="star" src="<?=base?>files\img\star.png" width="13" height="12" alt="4 Stars"></a></li>
                <li><a href="..\index-3.html?typ=hotel&amp;stars=5"><img class="star" src="<?=base?>files\img\star.png" width="13" height="12" alt="5 Stars"><img class="star" src=<?=base?>files\img\star.png" width="13" height="12" alt="5 Stars"><img class="star" src="<?=base?>files\img\star.png" width="13" height="12" alt="5 Stars"><img class="star" src="<?=base?>files\img\star.png" width="13" height="12" alt="5 Stars"><img class="star" src="<?=base?>files\img\star.png" width="13" height="12" alt="5 Stars"></a></li>
           
            </ul>
            
            <!--<h3>Preisklasse</h3>
        	<ul class="last small">
            	<li><a href="/content/?typ=hotel&amp;preis=100">&lt; CHF 100,-/ Nacht</a></li>
                <li><a href="/content/?typ=hotel&amp;preis=250">&lt; CHF 250,-/ Nacht</a></li>
                <li><a href="/content/?typ=hotel&amp;preis=500">&lt; CHF 500,-/ Nacht</a></li>
                <li><a href="/content/?typ=hotel&amp;preis=>500">> CHF 500,-/ Nacht</a></li>
           
            </ul>
            -->
        </div>
        
        <div id="categoriesBtn">
        	<div class="btn"><img class="cat" src="<?=base?>files\img\categories_de.png" alt="Kategorien"></div>
           <!-- <div class="btn"><a href="/content/ru/hotels-buchen.html"><img src="/files/img/search_de.png"  alt="Hotel finden"/></a></div>
        	-->
        </div>

                    
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
  <script type="text/javascript">
 
  TweenMax.to( $("#map"), 0, {css:{opacity:"0"}});
    
    var locations = { luzern: [47.0184922, 8.301251]},
      main = 'luzern',
      defaultZoom = 13;
     
      
     $('#map').each(function () {
      var mapOptions = {
            zoom: defaultZoom,
            disableDefaultUI: true,
			scrollwheel: true,

            center: new google.maps.LatLng(47.0184922, 8.331251)
            
          },
          map = new google.maps.Map(this, mapOptions),
          ll_locations = {},
          markers = {};
    
      $.each(locations, function (key, coords) {
        ll_locations[key] = new google.maps.LatLng(coords[0], coords[1]);
      });

      $.each(ll_locations, function (key, pos) {
        markers[key] = new google.maps.Marker({position: pos, map: map,icon: '/files/img/marker.png'});
      });
      
      map.mapTypes.set('gray_map',
        new google.maps.StyledMapType([{ stylers: [ { saturation: 0 } ]}],
                                      {name: "Gray Map"}));
      map.setMapTypeId('gray_map');
	  
      

        $('.route form').submit(function () {
            var url = 'http://google.de/maps';
            url += '?daddr=Belpbergstrasse 2, 3123 Belp';
            url += '&saddr='+ encodeURI($.trim($('input#destination').val()));
            window.open(url);
            return false;
          });
        
        return false;
      });
    
    
	TweenMax.to( $("#map"), 1, {css:{opacity:"1"}});
    </script>
            
            
            

