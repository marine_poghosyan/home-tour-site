<div id="imagegrid">
    <script type="text/javascript">
        gridtype = "home";

    </script>

    <div class="item big ">
		<a href="">
			<div class="info">
				<div class="text">	Garni temple <br>
				<span class="small">heathen culture </span>
				<span class="orange small"> / </span>
				<span class="small">Garni</span>
				</div>
			</div>
			<img width="800" src="files\img\grey.jpg" alt=" " data-original="files/tours/garni-temple.jpg?rex_img_type=detail_image&amp;rex_img_file=shu_201706_0908.jpg">
		</a>
	</div>

	<div class="item biginfo">
		<div class="biginfotext">
			<h1>ABOUT Garni temple <br>
				<span style="color: #dc5d13;">Example of heathen culture in Armenia. </span>
			</h1>
			<p>
           Garni temple which towers over a triangular cape, is the unique survived example of heathen culture in Armenia. It is a blend of Greco-Roman and Armenian styles. King Trdat the First ordered building the temple in the first century A.D. and dedicated it to the God of Sun. After adopting Christianity in 301 the pagan temple lost its significance and the fortress of Garni became the summer residence of the kings. Nowadays the ruins of the royal palace and the bathroom with a stunning mosaic work can be found near the temple.

		   </p>

        </div>

            <a href="content\ru\golfangebote.html"><div class="eventtip">
				<div class="tiplogo">
					<img class="biginfo" src="files\img\eventtip_de.png">
				</div>
                <div class="logodiv">
					<img class="logo biginfo" src="index.png?rex_img_type=highlight_logo&amp;rex_img_file=148.png">
				</div>
				<h2>Das weltweit prestigetr�chtigste Rennen auf der legend�rsten Rennstrecke findet am 26. - 27. Mai 2018 in Monaco statt. </h2>         
				<div class="open">
					<img class="biginfo" src="files\img\arrow_right.png">
				</div>
                 <div class="clearfix"></div>
                
                </div>
			</a>
        </div>
		
		<!--small item 1-->
		<div class="item ">
			<a href="tours\en\geghard">
				<div class="info">
					<div class="text">	Geghard monastery<br>
						<span class="small">cave monastery</span>
						<span class="orange small"> / </span>
						<span class="small">Geghardavank </span>
					</div>
				</div>
				<img width="800" src="files\img\grey.jpg" alt=" Bergisch Gladbach / Deutschland" data-original="files/tours/geghard.jpg?rex_img_type=detail_image&amp;rex_img_file=1_11_nachtansicht.jpg">
			</a>
		</div>

		<!--small item 2-->
		<div class="item ">
			<a href="tours\en\tsaghkadzor">
				<div class="info">
					<div class="text">	Tsaghkadzor <br>
						<span class="small"> Valley of flowers</span>
						<span class="orange small"> / </span>
						<span class="small"> winter resorts</span>
					</div>
				</div>
				<img width="800" src="files\img\grey.jpg" alt=" Samnaun / Schweiz" data-original="files/tours/tsaghkadzor.jpg?rex_img_type=detail_image&amp;rex_img_file=chasamontana_sommer.jpg">
			</a>
		</div>
		

		<!--small item 3-->	
		<div class="item ">
			<a href="content\ru\alle\afrika\suedafrika\madikwe-safari-lodge.html">
				<div class="info">
					<div class="text">	Echmiadzin <br>
						<span class="small">Mother cathedral</span>
						<span class="orange small"> / </span>
						<span class="small">Echmiadzin city</span>
					</div>
				</div>
				<img width="800" src="files\img\grey.jpg" alt=" Madikwe / S�dafrika" data-original="files/tours/echmiadzin.jpg?rex_img_type=detail_image&amp;rex_img_file=7_3.jpg">
			</a>
		</div>



	<!--small item 4-->
		<div class="item ">
			<a href="content\ru\alle\europa\england\waldorf-astoria-edinburgh-the-caledonian.html">
				<div class="info">
					<div class="text">	Sevan<br>
						<span class="small">Sevanavank</span>
						<span class="orange small"> / </span>
						<span class="small">Sevan ciy</span>
					</div>
				</div>
				<img width="800" src="files\img\grey.jpg" alt=" Edinburgh / Vereinigtes K�nigsreich" data-original="files/tours/sevan.png?rex_img_type=detail_image&amp;rex_img_file=exterior1_hr_1.jpg">
			</a>
		</div>


		<!--small item 5-->
		<div class="item ">
			<a href="content\ru\alle\europa\england\waldorf-astoria-edinburgh-the-caledonian.html">
				<div class="info">
					<div class="text">	Dilijan<br>
						<span class="small">Sevanavank</span>
						<span class="orange small"> / </span>
						<span class="small">Dilijan ciy</span>
					</div>
				</div>
				<img width="800" src="files\img\grey.jpg" alt=" Edinburgh / Vereinigtes K�nigsreich" data-original="files/tours/dilijan.jpg?rex_img_type=detail_image&amp;rex_img_file=exterior1_hr_1.jpg">
			</a>
		</div>


		<!--small item 6-->
		<div class="item ">
			<a href="content\ru\alle\europa\england\waldorf-astoria-edinburgh-the-caledonian.html">
				<div class="info">
					<div class="text">	Goshavank<br>
						<span class="small">fabulist Mkhitar Gosh</span>
						<span class="orange small"> / </span>
						<span class="small">Goshavank</span>
					</div>
				</div>
				<img width="800" src="files\img\grey.jpg" alt=" Edinburgh / Vereinigtes K�nigsreich" data-original="files/tours/goshavank.jpg?rex_img_type=detail_image&amp;rex_img_file=exterior1_hr_1.jpg">
			</a>
		</div>

		<!--small item 7-->
		<div class="item ">
			<a href="content\ru\alle\europa\england\waldorf-astoria-edinburgh-the-caledonian.html">
				<div class="info">
					<div class="text">	Haghartsin<br>
						<span class="small">Haghartsin monastery </span>
						<span class="orange small"> / </span>
						<span class="small">St. Stepanos church</span>
					</div>
				</div>
				<img width="800" src="files\img\grey.jpg" alt=" Edinburgh / Vereinigtes K�nigsreich" data-original="files/tours/haghartsin.jpg?rex_img_type=detail_image&amp;rex_img_file=exterior1_hr_1.jpg">
			</a>
		</div>

		<!--small item 8-->
		<div class="item ">
			<a href="content\ru\alle\europa\england\waldorf-astoria-edinburgh-the-caledonian.html">
				<div class="info">
					<div class="text">	Khor Virap <br>
						<span class="small">17th century </span>
						<span class="orange small"> / </span>
						<span class="small">view of the Biblical Mount Ararat</span>
					</div>
				</div>
				<img width="800" src="files\img\grey.jpg" alt=" Edinburgh / Vereinigtes K�nigsreich" data-original="files/tours/khorvirap.jpg?rex_img_type=detail_image&amp;rex_img_file=exterior1_hr_1.jpg">
			</a>
		</div>

		<!--small item 9-->
		<div class="item ">
			<a href="content\ru\alle\europa\england\waldorf-astoria-edinburgh-the-caledonian.html">
				<div class="info">
					<div class="text">	Noravank <br>
						<span class="small">13th century </span>
						<span class="orange small"> / </span>
						<span class="small">two-storey church</span>
					</div>
				</div>
				<img width="800" src="files\img\grey.jpg" alt=" Edinburgh / Vereinigtes K�nigsreich" data-original="files/tours/noravanq.jpg?rex_img_type=detail_image&amp;rex_img_file=exterior1_hr_1.jpg">
			</a>
		</div>

		<!--small item 10-->
		<div class="item ">
			<a href="content\ru\alle\europa\england\waldorf-astoria-edinburgh-the-caledonian.html">
				<div class="info">
					<div class="text">	Sanahin <br>
						<span class="small">Debed River </span>
						<span class="orange small"> / </span>
						<span class="small">Holy Mother of God</span>
					</div>
				</div>
				<img width="800" src="files\img\grey.jpg" alt=" Edinburgh / Vereinigtes K�nigsreich" data-original="files/tours/sanahin.jpg?rex_img_type=detail_image&amp;rex_img_file=exterior1_hr_1.jpg">
			</a>
		</div>






</div>