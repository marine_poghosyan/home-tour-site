﻿
gggggggggggggggssssssssssssssssssssssssssssss
<script type="text/javascript">
    gridtype="detail";
</script>

<div id="imagegrid" class="">


    <div class="item big">
        <a href="..\..\..\..\..\files\tours\geghard\1.jpg?rex_img_type=detailbig_image&amp;rex_img_file=1_11_nachtansicht.jpg" rel="Image" class="grouped_elements" title="">
            <div class="zoom"></div>
            <img title="" alt="" src="..\..\..\..\..\files\img\grey.jpg" data-original="..\..\..\..\..\files\tours\geghard\1.jpg?rex_img_type=detail_image_copy&amp;rex_img_file=1_11_nachtansicht.jpg">
        </a>
    </div>



    <div class="item">
        <a href="..\..\..\..\..\files\tours\geghard\2.jpg?rex_img_type=detailbig_image&amp;rex_img_file=dscf6742.jpg" rel="Image" class="grouped_elements" title="Salvador Dali Bar">
            <div class="zoom"></div>
            <img title="Salvador Dali Bar" alt="" src="..\..\..\..\..\files\img\grey.jpg" data-original="..\..\..\..\..\files\tours\geghard\2.jpg?rex_img_type=detailh_image&amp;rex_img_file=dscf6742.jpg">
         </a>
    </div>



    <div class="item">
        <a href="..\..\..\..\..\files\tours\geghard\2.jpg?rex_img_type=detailbig_image&amp;rex_img_file=dscf6742.jpg" rel="Image" class="grouped_elements" title="Salvador Dali Bar">
            <div class="zoom"></div>
            <img title="Salvador Dali Bar" alt="" src="..\..\..\..\..\files\img\grey.jpg" data-original="..\..\..\..\..\files\tours\geghard\2.jpg?rex_img_type=detailh_image&amp;rex_img_file=dscf6742.jpg">
        </a>
    </div>



    <div class="item">
        <a href="..\..\..\..\..\files\tours\geghard\2.jpg?rex_img_type=detailbig_image&amp;rex_img_file=dscf6742.jpg" rel="Image" class="grouped_elements" title="Salvador Dali Bar">
            <div class="zoom"></div>
            <img title="Salvador Dali Bar" alt="" src="..\..\..\..\..\files\img\grey.jpg" data-original="..\..\..\..\..\files\tours\geghard\2.jpg?rex_img_type=detailh_image&amp;rex_img_file=dscf6742.jpg">
        </a>
    </div>


    <div class="item">
        <a href="..\..\..\..\..\files\tours\geghard\2.jpg?rex_img_type=detailbig_image&amp;rex_img_file=dscf6742.jpg" rel="Image" class="grouped_elements" title="Salvador Dali Bar">
            <div class="zoom"></div>
            <img title="Salvador Dali Bar" alt="" src="..\..\..\..\..\files\img\grey.jpg" data-original="..\..\..\..\..\files\tours\geghard\2.jpg?rex_img_type=detailh_image&amp;rex_img_file=dscf6742.jpg">
        </a>
    </div>



    <div class="item">
        <a href="..\..\..\..\..\files\tours\geghard\2.jpg?rex_img_type=detailbig_image&amp;rex_img_file=dscf6742.jpg" rel="Image" class="grouped_elements" title="Salvador Dali Bar">
            <div class="zoom"></div>
            <img title="Salvador Dali Bar" alt="" src="..\..\..\..\..\files\img\grey.jpg" data-original="..\..\..\..\..\files\tours\geghard\2.jpg?rex_img_type=detailh_image&amp;rex_img_file=dscf6742.jpg">
        </a>
    </div>


    <div class="item">
        <a href="..\..\..\..\..\files\tours\geghard\2.jpg?rex_img_type=detailbig_image&amp;rex_img_file=dscf6742.jpg" rel="Image" class="grouped_elements" title="Salvador Dali Bar">
            <div class="zoom"></div>
            <img title="Salvador Dali Bar" alt="" src="..\..\..\..\..\files\img\grey.jpg" data-original="..\..\..\..\..\files\tours\geghard\2.jpg?rex_img_type=detailh_image&amp;rex_img_file=dscf6742.jpg">
        </a>
    </div>



    <div class="item">
        <a href="..\..\..\..\..\files\tours\geghard\2.jpg?rex_img_type=detailbig_image&amp;rex_img_file=dscf6742.jpg" rel="Image" class="grouped_elements" title="Salvador Dali Bar">
            <div class="zoom"></div>
            <img title="Salvador Dali Bar" alt="" src="..\..\..\..\..\files\img\grey.jpg" data-original="..\..\..\..\..\files\tours\geghard\2.jpg?rex_img_type=detailh_image&amp;rex_img_file=dscf6742.jpg">
        </a>
    </div>

 </div>



    <div id="detailtext">
        <div class="tip">
		    <img src="..\..\..\..\..\files\img\tip3s.png" alt="Lifestyle-Hotel">
	    </div>

        <div class="clearfix"></div>
        <div class="close">
            <a href="">
                <img src="..\..\..\..\..\files\img\close.png" alt="close">
            </a>
        </div>
        <h1>Geghard</h1>
        <h2 class="white top">Geghardavank </h2>
        <div class="short">
            <p>
                Geghard monastery is the unsurpassable masterpiece of the 13th century Armenian architecture. Some of the churches of the complex are masterfully hewn in a huge rock. From the outset the complex was called Ayrivank (cave monastery), later it was renamed Geghard (lance), as the lance used by the roman soldier to sting Jesus Christ's side, was kept in this monastery for many centuries. Due to its construction Geghardavank serves the best place for singing spiritual songs. The complex is rich in subtle sculptural embellishments and many striking khachkars (cross-stones).
            </p>
        </div>


        <div class="hiddendetail"><br>

            <p><strong>Gastronomie</strong><br>
                Die Küche verspricht Gaumenfreuden auf Weltklasse-Niveau – und das in drei verschiedenen Variationen. <br>
                Im <strong>Drei-Sterne-Restaurant Vendôme</strong> werden Feinschmecker der modernen Küche von Sternekoch Joachim Wissler verwöhnt. Das Restaurant zählt zu den besten der Welt und ist Deutschlands Nummer eins. In der von Weinkennern geschätzten <strong>Trattoria Enoteca</strong> kommt nur das Beste aus Italien auf den Teller und der sonntägliche Champagnerbrunch im <strong>Restaurant Jan Wellem</strong> lädt zum genussvollen Schlemmen ein. Die modern-klassische <strong>Salvador Dalí Bar</strong> überrascht mit einem innovativen Konzept &#8220;New fashioned food &amp; open wines&#8221; und lädt zum Verweilen in entspannter Atmosphäre ein.
            </p>

            <p><strong>Wellness &amp; Spa</strong><br>
                Das <strong>4 elements spa by Althoff Wellnesshotel</strong> bietet auf 1000 m2 eine Oase der Ruhe. Für Spa-Anwendungen stehen 8 Behandlungsräume zur Verfügung. Zu den weiteren Einrichtungen zählen eine Saunalandschaft mit Pool sowie ein Fitness-Bereich mit modernsten Geräten.
            </p>

            <p><strong>Tagen &amp; Feiern</strong><br>
                Das <strong>Grandhotel Schloss Bensberg</strong> mit seinem romantischen Innenhof und seinem Ballsaal bietet den perfekten Rahmen für traumhafte Hochzeiten. Tagen und feiern Sie in den historischen Schlossräumlichkeiten &#8211; durch flexible und individuelle Gestaltungsmöglichkeiten findet sich der passende Raum für jeden Anlass.
            </p>


            <iframe class="youtube-player" width="400" height="225" src="http://www.youtube.com/embed/ur0yW4epJOY"></iframe>
            <br><br>                <br>
            <div class="detailinfo">
                <div class="leftcol">Kategorie</div>
                <div class="rightcol">
                    <img class="star" src="..\..\..\..\..\files\img\star.png" width="13" height="12" alt="Kategorie">
                    <img class="star" src="..\..\..\..\..\files\img\star.png" width="13" height="12" alt="Kategorie">
                    <img class="star" src="..\..\..\..\..\files\img\star.png" width="13" height="12" alt="Kategorie">
                    <img class="star" src="..\..\..\..\..\files\img\star.png" width="13" height="12" alt="Kategorie">
                    <img class="star" src="..\..\..\..\..\files\img\star.png" width="13" height="12" alt="Kategorie">
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="detailinfo">
                <div class="leftcol">Preisklasse</div>
                <div class="rightcol"></div>
                <div class="clearfix"></div>
            </div>

            <div class="detailinfo">
                <div class="leftcol">Land</div>
                <div class="rightcol">Deutschland</div>
                <div class="clearfix"></div>
            </div>



            <br>
            <br>
                        <!-- AddThis Button BEGIN -->
            <div class="addthis_toolbox addthis_default_style ">
                <a class="addthis_button_compact" style="padding-right:10px; font-size:13px;">Teilen</a>
                <a class="addthis_button_google_plusone" style="padding-right:10px;" g:plusone:count="false"></a>
                <a class="addthis_button_linkedin_counter" style="padding-right:10px;" li:counter="none"></a>
                <a class="addthis_button_facebook_like" style="padding-right:10px;" fb:like:layout="button_count" fb:like:action="recommend"></a>
                <a class="addthis_button_tweet" tw:via="addthis"></a>
            <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5253fbfb5a1905c9"></script>

            </div>

        </div>

        <div class="button buttonO" data-o="Detail schliessen" data-c="Hotelbeschreibung">Hotelbeschreibung
            <img src="..\..\..\..\..\files\img\arrow_down.png" alt="Hotelbeschreibung">
        </div>

    </div>


    <div id="categoriesBtn">
        <div class="btn"><img class="cat" src="..\..\..\..\..\files\img\categories_de.png" alt="Kategorien"></div>
       <!-- <div class="btn"><a href="/content/ru/hotels-buchen.html"><img src="/files/img/search_de.png"  alt="Hotel finden"/></a></div>
        -->
    </div>









