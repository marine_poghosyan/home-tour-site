﻿<script type="text/javascript">
    gridtype="detail";
</script>

<div id="imagegrid" class="">

    <div class="item big">
        <a href="<?=base?>\files\tours\geghard\1.jpg?rex_img_type=detailbig_image&amp;rex_img_file=1_11_nachtansicht.jpg" rel="Image" class="grouped_elements" title="">
            <div class="zoom"></div>
            <img title="" alt="" src="<?=base?>\files\img\grey.jpg" data-original="<?=base?>\files\tours\geghard\1.jpg?rex_img_type=detail_image_copy&amp;rex_img_file=1_11_nachtansicht.jpg">
        </a>
    </div>



    <div class="item">
        <a href="<?=base?>\files\tours\geghard\4.jpg?rex_img_type=detailbig_image&amp;rex_img_file=dscf6742.jpg" rel="Image" class="grouped_elements" title="Salvador Dali Bar">
            <div class="zoom"></div>
            <img title="Salvador Dali Bar" alt="" src="<?=base?>\files\img\grey.jpg" data-original="<?=base?>\files\tours\geghard\4.jpg?rex_img_type=detailh_image&amp;rex_img_file=dscf6742.jpg">
        </a>
    </div>



    <div class="item">
        <a href="<?=base?>\files\tours\geghard\2.jpg?rex_img_type=detailbig_image&amp;rex_img_file=dscf6742.jpg" rel="Image" class="grouped_elements" title="Salvador Dali Bar">
            <div class="zoom"></div>
            <img title="Salvador Dali Bar" alt="" src="<?=base?>\files\img\grey.jpg" data-original="<?=base?>\files\tours\geghard\2.jpg?rex_img_type=detailh_image&amp;rex_img_file=dscf6742.jpg">
        </a>
    </div>



    <div class="item">
        <a href="<?=base?>\files\tours\geghard\3.jpg?rex_img_type=detailbig_image&amp;rex_img_file=dscf6742.jpg" rel="Image" class="grouped_elements" title="Salvador Dali Bar">
            <div class="zoom"></div>
            <img title="Salvador Dali Bar" alt="" src="<?=base?>\files\img\grey.jpg" data-original="<?=base?>\files\tours\geghard\3.jpg?rex_img_type=detailh_image&amp;rex_img_file=dscf6742.jpg">
        </a>
    </div>


    <div class="item">
        <a href="<?=base?>\files\tours\geghard\4.jpg?rex_img_type=detailbig_image&amp;rex_img_file=dscf6742.jpg" rel="Image" class="grouped_elements" title="Salvador Dali Bar">
            <div class="zoom"></div>
            <img title="Salvador Dali Bar" alt="" src="<?=base?>\files\img\grey.jpg" data-original="<?=base?>\files\tours\geghard\4.jpg?rex_img_type=detailh_image&amp;rex_img_file=dscf6742.jpg">
        </a>
    </div>



    <div class="item">
        <a href="<?=base?>\files\tours\geghard\5.jpg?rex_img_type=detailbig_image&amp;rex_img_file=dscf6742.jpg" rel="Image" class="grouped_elements" title="Salvador Dali Bar">
            <div class="zoom"></div>
            <img title="Salvador Dali Bar" alt="" src="<?=base?>\files\img\grey.jpg" data-original="<?=base?>\files\tours\geghard\5.jpg?rex_img_type=detailh_image&amp;rex_img_file=dscf6742.jpg">
        </a>
    </div>


    <div class="item">
        <a href="<?=base?>\files\tours\geghard\6.jpg?rex_img_type=detailbig_image&amp;rex_img_file=dscf6742.jpg" rel="Image" class="grouped_elements" title="Salvador Dali Bar">
            <div class="zoom"></div>
            <img title="Salvador Dali Bar" alt="" src="<?=base?>\files\img\grey.jpg" data-original="<?=base?>\files\tours\geghard\6.jpg?rex_img_type=detailh_image&amp;rex_img_file=dscf6742.jpg">
        </a>
    </div>



    <div class="item">
        <a href="<?=base?>\files\tours\geghard\3.jpg?rex_img_type=detailbig_image&amp;rex_img_file=dscf6742.jpg" rel="Image" class="grouped_elements" title="Salvador Dali Bar">
            <div class="zoom"></div>
            <img title="Salvador Dali Bar" alt="" src="<?=base?>\files\img\grey.jpg" data-original="<?=base?>\files\tours\geghard\3.jpg?rex_img_type=detailh_image&amp;rex_img_file=dscf6742.jpg">
        </a>
    </div>

</div>

<div id="detailtext">
    <div class="tip">
        <img src="<?=base?>\files\img\tip3s.png" alt="Lifestyle-Hotel">
    </div>

    <div class="clearfix"></div>
    <div class="close">
        <a href="<?=base?>tours\en\geghard">
            <img src="<?=base?>\files\img\close.png" alt="close">
        </a>
    </div>
    <h1>Geghard</h1>
    <h2 class="white top">Geghardavank </h2>
    <div class="short">
        <p>
            Geghard monastery is the unsurpassable masterpiece of the 13th century Armenian architecture. Some of the churches of the complex are masterfully hewn in a huge rock. From the outset the complex was called Ayrivank (cave monastery), later it was renamed Geghard (lance), as the lance used by the roman soldier to sting Jesus Christ's side, was kept in this monastery for many centuries. Due to its construction Geghardavank serves the best place for singing spiritual songs. The complex is rich in subtle sculptural embellishments and many striking khachkars (cross-stones).
        </p>
    </div>


    <div class="hiddendetail"><br>

        <p><strong>Complex</strong><br>
            Today the monastery complex is located at the end of the paved road, and the walk up from the parking lot is lined with women selling sweet bread, sheets of dried fruit (fruit lavash), sweet sujukh (grape molasses covered strings of walnuts) and various souvenirs. A group of musicians usually plays for a few seconds as visitors approach, perhaps willing to play longer for money.
            At the approach to the main entrance on the west there are small caves, chapels, carvings and constructions on the hillside. Right before the entrance are some shallow shelves in the cliff onto which people try to throw pebbles in order to make their wish come true. Just inside the entrance to the compound are the 12th-13th century ramparts protecting three sides of the complex, and the cliffs behind protect the fourth. Walking across the complex will take one to the secondary entrance on the east, outside of which is a table for ritual animal offerings (matagh), and a bridge over the stream.
            The one- and two-storey residential and service structures situated on the perimeter of the monastery’s yard were repeatedly reconstructed, sometimes from their foundations, as happened in the 17th century and in 1968—1971. It is known that most of the monks lived in cells excavated into the rock-face outside the main enceinte, which have been preserved, along with some simple oratories. The rock-faces over the whole area bear elaborate crosses (khatchkar) carved in relief. More than twenty spaces, varying in shape and size, were carved, at different levels, in solid rock massifs surrounding the main cave structures.
        </p>

        <p><strong>The Katoghike Chapel</strong><br>
            Though there are inscriptions dating to the 1160s, the main church was built in 1215 under the auspices of the brothers Zakare and Ivane (of the Zakarid-Mkhargrzeli family), the generals of Queen Tamar of Georgia, who took back most of Armenia from the Turks. This is the main church of the complex, and traditional in most respects. This church is built against the mountain, which is not exposed even in the interior. The plan forms an equal-armed cross, inscribed in a square and covered with a dome on a square base. In the corners there are small barrel-vaulted two-storey chapels with steps protruding from the wall. The internal walls have many inscriptions recording donations.

        </p>


        <iframe class="youtube-player" width="400" height="225" src="https://www.youtube.com/embed/UvrXXo_LVz4"></iframe>
        <br><br>                <br>



    </div>

    <div class="button buttonO" data-o="Less" data-c="Details">Details
        <img src="<?=base?>\files\img\arrow_down.png" alt="Details">
    </div>


</div>