﻿<script type="text/javascript">
    gridtype="detail";
</script>

<div id="imagegrid" class="">

    <div class="item big">
        <a href="<?=base?>\files\tours\tsaghkadzor\1.jpg?rex_img_type=detailbig_image&amp;rex_img_file=1_11_nachtansicht.jpg" rel="Image" class="grouped_elements" title="">
            <div class="zoom"></div>
            <img title="" alt="" src="<?=base?>\files\img\grey.jpg" data-original="<?=base?>\files\tours\tsaghkadzor\1.jpg?rex_img_type=detail_image_copy&amp;rex_img_file=1_11_nachtansicht.jpg">
        </a>
    </div>



    <div class="item">
        <a href="<?=base?>\files\tours\tsaghkadzor\7.jpg?rex_img_type=detailbig_image&amp;rex_img_file=dscf6742.jpg" rel="Image" class="grouped_elements" title="Salvador Dali Bar">
            <div class="zoom"></div>
            <img title="Salvador Dali Bar" alt="" src="<?=base?>\files\img\grey.jpg" data-original="<?=base?>\files\tours\tsaghkadzor\7.jpg?rex_img_type=detailh_image&amp;rex_img_file=dscf6742.jpg">
        </a>
    </div>



    <div class="item">
        <a href="<?=base?>\files\tours\tsaghkadzor\2.jpg?rex_img_type=detailbig_image&amp;rex_img_file=dscf6742.jpg" rel="Image" class="grouped_elements" title="Salvador Dali Bar">
            <div class="zoom"></div>
            <img title="Salvador Dali Bar" alt="" src="<?=base?>\files\img\grey.jpg" data-original="<?=base?>\files\tours\tsaghkadzor\2.jpg?rex_img_type=detailh_image&amp;rex_img_file=dscf6742.jpg">
        </a>
    </div>



    <div class="item">
        <a href="<?=base?>\files\tours\tsaghkadzor\3.jpg?rex_img_type=detailbig_image&amp;rex_img_file=dscf6742.jpg" rel="Image" class="grouped_elements" title="Salvador Dali Bar">
            <div class="zoom"></div>
            <img title="Salvador Dali Bar" alt="" src="<?=base?>\files\img\grey.jpg" data-original="<?=base?>\files\tours\tsaghkadzor\3.jpg?rex_img_type=detailh_image&amp;rex_img_file=dscf6742.jpg">
        </a>
    </div>


    <div class="item">
        <a href="<?=base?>\files\tours\tsaghkadzor\4.jpg?rex_img_type=detailbig_image&amp;rex_img_file=dscf6742.jpg" rel="Image" class="grouped_elements" title="Salvador Dali Bar">
            <div class="zoom"></div>
            <img title="Salvador Dali Bar" alt="" src="<?=base?>\files\img\grey.jpg" data-original="<?=base?>\files\tours\tsaghkadzor\4.jpg?rex_img_type=detailh_image&amp;rex_img_file=dscf6742.jpg">
        </a>
    </div>



    <div class="item">
        <a href="<?=base?>\files\tours\tsaghkadzor\5.jpg?rex_img_type=detailbig_image&amp;rex_img_file=dscf6742.jpg" rel="Image" class="grouped_elements" title="Salvador Dali Bar">
            <div class="zoom"></div>
            <img title="Salvador Dali Bar" alt="" src="<?=base?>\files\img\grey.jpg" data-original="<?=base?>\files\tours\tsaghkadzor\5.jpg?rex_img_type=detailh_image&amp;rex_img_file=dscf6742.jpg">
        </a>
    </div>


    <div class="item">
        <a href="<?=base?>\files\tours\tsaghkadzor\6.jpg?rex_img_type=detailbig_image&amp;rex_img_file=dscf6742.jpg" rel="Image" class="grouped_elements" title="Salvador Dali Bar">
            <div class="zoom"></div>
            <img title="Salvador Dali Bar" alt="" src="<?=base?>\files\img\grey.jpg" data-original="<?=base?>\files\tours\tsaghkadzor\6.jpg?rex_img_type=detailh_image&amp;rex_img_file=dscf6742.jpg">
        </a>
    </div>



    <div class="item">
        <a href="<?=base?>\files\tours\tsaghkadzor\3.jpg?rex_img_type=detailbig_image&amp;rex_img_file=dscf6742.jpg" rel="Image" class="grouped_elements" title="Salvador Dali Bar">
            <div class="zoom"></div>
            <img title="Salvador Dali Bar" alt="" src="<?=base?>\files\img\grey.jpg" data-original="<?=base?>\files\tours\tsaghkadzor\3.jpg?rex_img_type=detailh_image&amp;rex_img_file=dscf6742.jpg">
        </a>
    </div>

    <div class="item">
        <a href="<?=base?>\files\tours\tsaghkadzor\1.jpg?rex_img_type=detailbig_image&amp;rex_img_file=dscf6742.jpg" rel="Image" class="grouped_elements" title="Salvador Dali Bar">
            <div class="zoom"></div>
            <img title="Salvador Dali Bar" alt="" src="<?=base?>\files\img\grey.jpg" data-original="<?=base?>\files\tours\tsaghkadzor\1.jpg?rex_img_type=detailh_image&amp;rex_img_file=dscf6742.jpg">
        </a>
    </div>

</div>

<div id="detailtext">
    <div class="tip">
        <img src="<?=base?>\files\img\tip3s.png" alt="Lifestyle-Hotel">
    </div>

    <div class="clearfix"></div>
    <div class="close">
        <a href="">
            <img src="<?=base?>\files\img\close.png" alt="close">
        </a>
    </div>
    <h1>Tsaghkadzor</h1>
    <h2 class="white top">Valley of flowers/winter resorts</h2>
    <div class="short">
        <p>
            Tsaghkadzor literally means valley of flowers or flower canyon in Armenian. The name of Tsaghkadzor is associated with the name of the nearby Tsaghkunyats Mountains, located to the west of the town.

            Originally, it was known as Tsaghkunyats Dzor (Ծաղկունյաց ձոր) by its foundation during the 3rd century. In the 11th century, the town was known as Kecharuyk (Կեչառույք) or Kecharis (Armenian: Կեչառիս) derived from the Kecharis Principality under the Armenian Pahlavuni family. Later, during the 17th century, the towns was called Darachichak by the Turkic invaders, keeping the name until 1947, when it was renamed Tsaghkadzor.
        </p>
    </div>


    <div class="hiddendetail"><br>

        <p><strong>Culture</strong><br>
            Tsaghkadzor has a cultural palace and a public library. The day of Tsaghkadzor is celebrated annually on October 3.[6]

            The Kecharis Monastery is one of the significant religious complexes of Eastern Armenia and one of the well-preserved medieval architectural samples of the Armenian Highland. It was founded at the beginning of the 11th century, consisted of 4 separate adjacent churches. The main church of the complex is the Saint Gregory Church built in 1033. The church of Surp Nshan built in 1051, is located to the south of Saint Gregory Church. The Katoghike Church built at the beginning of the 13th century, stands to the south of Surp Nshan Church. The fourth church of the complex is the Church of Surp Harutyun of 1220.

            The House-museum of Brothers Orbeli, is dedicated to the Armenian scientists Ruben, Levon and Joseph Orbeli who were native of Tsaghkadzor. Professor Ruben Orbeli was the founder of marine archaeology and one of the major specialists of ocean engineering. Physiologist Levon Orbeli was a prominent member of the USSR and Armenian SSR academies of science. Scientist Joseph Orbeli was an orientalist specialized in medieval history of South Caucasus. He administered the Hermitage Museum of Saint Petersburg between 1934 and 1951.[7]
        </p>

        <p><strong>Transportation</strong><br>
            Tsaghkadzor is accessible through the H-5 Road from the southeast, as well as the H-28 Road from the northwest. The H-29 Road connects the town with Gegharkunik and northeastern Armenia, via the town of Hrazdan. Public transport to Tsaghkadzor is available from Hrazdan city and leaves from Micro District hourly until 19:30. Multiple private cabs are also available upon request. The regular price from Hrazdan to Tsaghkadzor is AMD 1,000 (approximately 2 dollars).
        </p>


        <iframe class="youtube-player" width="400" height="225" src="https://www.youtube.com/embed/Ylf2-26wPI0"></iframe>
        <br><br>                <br>



    </div>

    <div class="button buttonO" data-o="Less" data-c="Details">Details
        <img src="<?=base?>\files\img\arrow_down.png" alt="Details">
    </div>


</div>