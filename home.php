<div id="imagegrid">
    <script type="text/javascript">
        gridtype = "home";

    </script>

    <div class="item big ">
		<a href="hotels\en\abovyan">

			<div class="info">
				<div class="text">	Abovyan Apartment<br>
				<span class="small">Yerevan</span>
				<span class="orange small"> / </span>
				<span class="small">city center</span>
				</div>
			</div>
			<img width="800" src="<?=base?>files\img\grey.jpg" alt=" " data-original="<?=base?>files/apartment/k6_main.jpg?rex_img_type=detail_image&amp;rex_img_file=shu_201706_0908.jpg">
		</a>
	</div>

	<div class="item biginfo">
		<div class="biginfotext">
			<h1>ABOUT RENTAL APARTMENT<br>
				<span style="color: #dc5d13;">6 Koryun street, apartment 20, 0009 Yerevan Armenia</span>
			</h1>
			<p>
           This grand attic is located in the fundamental building in one of the oldest Yerevan avenues. The history of the building gets back to 1940. Central Luxury apartment in the heart of Yerevan with real fireplace for cold and romantic evenings! the apartment is 145 sq meters and is perfectly appointed for 1-8 persons, so you will enjoy a lot of space! The apartment is located on a top floor (5th) floor with no elevator, so if you have mobility problems it may not be for you. On top of the apartment there is a furnished terrace (20 m2) with a city view, including table and two armchairs where you can enjoy your breakfasts/dinners. The terrace is the ideal place to relax.The apartment was very recently renovated, mixing old and new, a 85 year old architecture along with new luxurious equipment and decor.</p>

			<a href="tel:+37494405055">
				<img class="biginfo" src="files\img\phone.png"> +374 94 405055
			</a>
			<a href="mailto:reservation@yerevan-sky.com">
				<img style="margin-left: 40px;" class="biginfo" src="<?=base?>files\img\mail.png">reservation@yerevan-sky.com
			</a>

        </div>

            <a href=""><div class="eventtip">
				<div class="tiplogo">
					<img class="biginfo" src="<?=base?>files\img\eventtip_de.png">
				</div>
                <div class="logodiv">
					<img class="logo biginfo" src="<?=base?>files\img\armenian-flag-map.png?rex_img_type=highlight_logo&amp;rex_img_file=148.png" width="70px" height="70px">
				</div>
				<h2>Founded in 782 BC, the city of about 1.1 million people is nestled in the shadow of Mount Ararat. </h2>
				<div class="open">
					<img class="biginfo" src="<?=base?>files\img\arrow_right.png">
				</div>
                 <div class="clearfix"></div>
                
                </div>
			</a>
        </div>
		
		<!--small item 1-->
		<div class="item ">
			<a href="hotels\en\yerevansky">
				<div class="info">
					<div class="text">	Yerevan Sky<br>
						<span class="small">Yerevan</span>
						<span class="orange small"> / </span>
						<span class="small">city center</span>
					</div>
				</div>
				<img width="800" src="<?=base?>files\img\grey.jpg" alt=" Yerevan Sky apartment" data-original="<?=base?>files/apartment/k7_home.jpg?rex_img_type=detail_image&amp;rex_img_file=1_11_nachtansicht.jpg">
			</a>
		</div>

		<!--small item 2-->
		<div class="item ">
			<a href="content\ru\alle\europa\schweiz\chasa-montana-hotel-und-spa.html">
				<div class="info">
					<div class="text">	Yerevan Sky<br>
						<span class="small">Rest room</span>
						<span class="orange small"> / </span>
						<span class="small">Bedroom</span>
					</div>
				</div>
				<img width="800" src="files\img\grey.jpg" alt=" Samnaun / Schweiz" data-original="files/apartment/k7_1.jpg?rex_img_type=detail_image&amp;rex_img_file=chasamontana_sommer.jpg">
			</a>
		</div>
		
		<!--small item 3-->
		<div class="item ">
			<a href="content\ru\alle\europa\england\waldorf-astoria-edinburgh-the-caledonian.html">
				<div class="info">
					<div class="text">	Yerevan sky<br>
						<span class="small">Eating place</span>
						<span class="orange small"> / </span>
						<span class="small">Kitchen</span>
					</div>
				</div>
				<img width="800" src="files\img\grey.jpg" alt=" Edinburgh / Vereinigtes K�nigsreich" data-original="files/apartment/k7_2.jpg?rex_img_type=detail_image&amp;rex_img_file=exterior1_hr_1.jpg">
			</a>
		</div>

		<!--small item 4-->	
		<div class="item ">
			<a href="content\ru\alle\afrika\suedafrika\madikwe-safari-lodge.html">
				<div class="info">
					<div class="text">	Yerevan sky<br>
						<span class="small">Play space</span>
						<span class="orange small"> / </span>
						<span class="small">Yard</span>
					</div>
				</div>
				<img width="800" src="files\img\grey.jpg" alt=" Madikwe / S�dafrika" data-original="files/apartment/k7_3.jpg?rex_img_type=detail_image&amp;rex_img_file=7_3.jpg">
			</a>
		</div>
</div>


