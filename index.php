<?php
define("base", "http://localhost/home-tour-site/");
$route = isset($_GET['route'])?addslashes($_GET['route']):'';
$gets = explode('/', $route);
/* Pages */
$path = '';
if(isset($gets) && $gets[0] != ''){
    if (isset($gets[2]) && $gets[0] == 'tours'){
        if (isset($gets[1])) {
            $path = "content/".$gets[1]."/tours/";
            $page = $path.$gets[2];
        }
    } else if (isset($gets[2]) && $gets[0] == 'hotels') {
        if (isset($gets[1])) {
            $path = "content/".$gets[1]."/hotel-types/";
            $page = $path.$gets[2];
        }

    } else {
        $page = $gets[0];
    }

}else{
    $page = 'home';
}
$page.= '.php';
?>

<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="google-site-verification" content="jzTZrVkr2qhU_YbGIpc3V8NfyTUzUOB5mhX6hcH3864">

    <title>Lifestyle-Hotels / handverlesene, einzigartige Hotels, Boutique Hotels, Design Hotels, Lodges, Resorts und Restaurants</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="keywords" content="Boutique Hotels,Design Hotels,Gesch�ftsreisen,Ferien,Hotels,Stadthotels,Business Hotels,Serviced Apartments,Luxus Hotels,4 Sterne Hotels,5 Sterne Hotels,hotels">
    <meta name="description" content="Lifestyle-Hotels kennt vom gehobenen Mittelklasse-Haus bis zum Luxushotel alle Partner pers�nlich. Dieser einzigartige Exklusiv-Service ist kostenlos und �bernimmt f�r Individualreisende und Gesch�ftsleute die Hotelsuche und Zimmerreservierung.">
    <link rel="canonical" href="index.html">
    <meta name="viewport" content="width=1024, user-scalable=no">

    <meta property="og:title" content="Lifestyle-Hotels / handverlesene, einzigartige Hotels, Boutique Hotels, Design Hotels, Lodges, Resorts und Restaurants">
    <meta property="og:description" content="Lifestyle-Hotels kennt vom gehobenen Mittelklasse-Haus bis zum Luxushotel alle Partner pers�nlich. Dieser einzigartige Exklusiv-Service ist kostenlos und �bernimmt f�r Individualreisende und Gesch�ftsleute die Hotelsuche und Zimmerreservierung.">
    <meta property="og:type" content="website">
    
    <meta property="og:image" content="http://lifestyle-hotels.ch/content/files/img/logof.jpg">
    <meta property="og:url" content="http://lifestyle-hotels.ch/">


    <link rel="stylesheet" href="<?=base?>\files\css\normalize.min.css">
    <link rel="stylesheet" href="<?=base?>\files\css\main.css?v=2">
    <link rel="stylesheet" href="<?=base?>\files\css\jquery-ui-1.10.2.custom.css">
    <link rel="stylesheet" href="<?=base?>\files\addons\gs_fancybox\jquery.fancybox-1.3.4.css">

    <link type="text/css" media="screen" rel="stylesheet" href="<?=base?>files\css\awwwards.css">

    <script src="<?=base?>\files\js\vendor\modernizr-2.6.2.min.js"></script>
    <script type="text/javascript">
        cur_lang = 0;
    </script>
</head>

	<body>


		<?php include 'navigation.php'; ?>

		<?php include 'filters.php'; ?>

        <?php include ($page); ?>


		<?php include 'categories.php'; ?>




        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="/files/js/vendor/jquery-1.8.3.min.js"><\/script>')</script>
        <script src="<?=base?>\files\js\vendor\jquery-ui-1.10.2.custom.min.js"></script>
		<script src="<?=base?>\files\js\vendor\TweenMax.min.js"></script>
        <script src="<?=base?>\files\js\vendor\jquery.form.js"></script>
        <script src="<?=base?>\files\js\plugins.js?v=2"></script>
        <script src="<?=base?>\files\js\main.js?v=2"></script>
        
        
       
        <script type="text/javascript" src="<?=base?>\files\addons\gs_fancybox\jquery.fancybox-1.3.4.js"></script>
        <script type="text/javascript" src="<?=base?>\files\addons\gs_fancybox\jquery.fancybox-1.3.4.pack.js"></script>
        <script type="text/javascript" src="<?=base?>\files\addons\gs_fancybox\jquery.easing-1.3.pack.js"></script>
   
        
         <script type="text/javascript">$("a.grouped_elements").fancybox({
		'transitionIn'	: 'elastic',
		'titlePosition':'inside',
		'transitionOut' 		: 'elastic',
		'overlayColor':'#000',
		onError: function(){
					$.fancybox.close();
			
		}
		});</script>
       
        
        <script type="text/javascript">
		
				
			danketext ='Vielen Dank f�r Ihr Interesse! Ihre Reservierungsanfrage wird umgehend beantwortet.';
			eingetragentext = 'Herzlichen Dank! Sie wurden in unseren Newsletterverteiler eingetragen!';
	
		</script>
        
        

    </body>
</html>